
// MFCRadioButtonDlg.h : ヘッダー ファイル
//

#pragma once
#include "afxwin.h"


// CMFCRadioButtonDlg ダイアログ
class CMFCRadioButtonDlg : public CDialogEx
{
// コンストラクション
public:
	CMFCRadioButtonDlg(CWnd* pParent = NULL);	// 標準コンストラクター

// ダイアログ データ
	enum { IDD = IDD_MFCRADIOBUTTON_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV サポート


// 実装
protected:
	HICON m_hIcon;

	// 生成された、メッセージ割り当て関数
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	int m_radio3_1;
};
